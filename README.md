
# Api template v2.0.0 - Web

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li>
      <a href="#dependências">Dependências</a>
    </li>
    <li>
      <a href="#setup-do-projeto">Setup do Projeto</a>
      <ul>
        <li><a href="#setup-api-only">Se for só api</a></li>
      </ul>
    </li>
    <li><a href="#usando-git-flow">Usando git-flow</a></li>
    <li><a href="#testes">Testes</a></li>
    <li><a href="#documentação-no-Postman">Documentação </a></li>
  </ol>
</details>


<!-- ABOUT THE PROJECT -->
------------------------
### Dependências
 - Ruby v3.0.1
 - Rails v6.1.3.2
 - MySQL v8.0.25-0
 - Git e Git Flow

 ------------------------

## Setup do Projeto
1. Ruby
   Caso não tenha Ruby intalado em sua máquina, recomendamos o uso do RVM para a instalação do mesmo.
2. Download
   Acesse via terminal o local dos seus projetos e faça o download do    repositório.
   ```bash
   git clone git@gitlab.com:jera-software/api-template.git
   ```

3. Apotando o git para o repositório certo
   Antes de começar a codar, você precisa substituir a origin do repositório da api-template pelo seu
   ```
   git remote remove origin
   git remote add origin <SEU_GIT>
   git push -f origin main
   git push -u origin --all
   git push -u origin --tags
   ```
4. Nome do app
   Substitua todas as ocorrências de 'api_template' nos arquivos pelo nome do seu app ( ctrl + shift + f )

5. Configurar o `config/database.yml`
   Primeiramente é necessário criar o arquivo de setup de banco:
   ```bash
   cp config/database{.example.yml,.yml}
   ```

6. Gemset e RubyVersion
   Dentro da pasta do projeto, crie os arquivos `.ruby-version` e `.   ruby-gemset` e depois entre novamente na pasta para carregar as novas    configurações. E verifique se a ruby_version no .gitlab-ci.yml e config/deploy.rb estão corretas (só olhar se são as mesmas do gemfile ;) )
   ```bash
   echo "<RUBY-VERSION>" > .ruby-version && echo "api_template" > .   ruby-gemset
   cd .
   ```
7. Instalar gems
   A ferramenta `bundle` instalará todas as gems que foram definidas no Gemset    do projeto.
   ```bash
   gem install bundle && bundle install
   ```

8. Rails db
   por fim, crei o banco, rode o as migrations e gere os seeds
   ```bash
   rails db:create db:migrate db:seed
   ```

9. Não se esqueça que você de **configurar o Sentry e o NewRelic**, se precisar de ajuda manda lá no time web ;)

10. Configurações finais
   - Você deve gerar a `master.key` do seu projeto e colocar ela no Git (fazer um commit) e depois adicionar no `.gitignore` de novo.

   - O credentials do projeto esta no gitignore, então adiciona ele apenas no servidor.

11. Docker (opcional)
   Caso queira utilizar docker em desenvolvimento, é só rodar o docker-compose do proejto
   ```bash
   docker-compose up
   docker-compose run application rake db:create db:migrate db:seed
   ```
<!-->======================== ATENÇÃO =================================<!-->
<!--> OE, caso seu projeto tenha front web, apague essa parte do README<!-->
### setup api only
Se sua aplicação **não** for só API e tiver uma parte WEB execute os seguintes passos:
  - Primerio la em `application.rb` troque a linha `config.api_only = true` por `config.api_only = false`;
  - Então descomente as gems de `Assets` la no Gemfile e rode um `bundle install` para instalar as dependencias necessarias;
  - Entao rode o comando `rails webpacker:install` que vai fazer todo o setup necessario pra seu front.
  - Para funcionar no ambiente de staging, você vai precisar criar na pasta do webpack um arquivo de `staging.js`, você pode se basear no `production.js` para fazer isso. (Leia mais sobre o Webpacker aqui https://github.com/rails/webpacker)

   - Se sua aplicação não tiver sidekiq, remova o arquivo `app/workers/base_worker.rb`

   - Na migration que cria o User foram criadas as seguintes linhas:

      ```ruby
       # Omniauth
       # t.string :provider, :null => false, :default => "email"
       # t.string :uid, :null => false, :default => ""
      ```

   - Elas são ciradas pelo devise pra usar facebook ou algum outro provider de login (ex. google, apple). Para entender melhor como funciona https://github.com/plataformatec/devise/wiki/OmniAuth:-Overview

------------------------
## Usando git-flow
Para utilização do ambiente de desenvolvimento configure o Git Flow.
```bash
$ sudo apt-get install git-flow
$ git flow init
```
Pressione Enter para todas as opções que serão exibidas. Depois disso você já estara na branch de development.

Caso opte por não utilizar Git Flow, é necessário fazer o download da branch `develop` remota.

------------------------

## Testes
Utilizamos o framework de testes Rspec e cucumber para execução de testes.

Manualmente: 
```bash
rake rspec
rake cucumber
```

Com Docker:
```bash
docker-compose up
docker-compose run application rake db:create db:migrate db:seed RAILS_ENV=test
docker-compose run application spring rspec
```

------------------------
## Documentação no Postman
Todo projeto de API deve ser documentado.
Durante o setup, é importante criar uma conta no Postman para o projeto.
- Não esqueça de editar os Merge Request Templates com o link da documentação :D
